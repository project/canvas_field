<?php
/**
 * Implementation of hook_widget_info().
 *
 */
function canvas_field_widget_info() {
  return array(
    'canvas_field' => array(
      'label' => t('Signature widget'),
      'field types' => array('canvas_field'),
      'multiple values' => CONTENT_HANDLE_CORE,
      'callbacks' => array(
        'default value' => CONTENT_CALLBACK_NONE,
      ),
    ),
  );
}

/**
 * Implementation of hook_widget_settings().
 */
function canvas_field_widget_settings($op, &$widget) {
  switch ($op) {
    case 'form':
      $form = array(
        'canvas_field_directory' => array(
            '#type' => 'textfield',
            '#title' => 'File Path',
            '#description' => 'Enter a directory to save captured canvas fields to.  Ex: <strong>sites/default/files</strong>',
            '#default_value' => $widget['canvas_field_directory'],
        ),
        'canvas_size' => array(
          '#type' => 'fieldset',
          '#title' => 'Canvas Size',
          '#description' => 'Enter the size of the canvas.',
          'canvas_width' => array(
            '#type' => 'textfield',
            '#title' => 'W',
            '#size' => 2,
            '#default_value' => $widget['canvas_width'],
           ),
           'canvas_height' => array(
              '#type' => 'textfield',
              '#title' => 'H',
              '#size' => 2,
              '#default_value' => $widget['canvas_height'],
           ),
        ),
      );
      return $form;
    case 'validate':
      $widget['canvas_field_directory'] = file_create_path($widget['canvas_field_directory']);
    case 'save':
      return array('canvas_field_directory', 'canvas_width', 'canvas_height');
  }
}

/**
 * Implementation of hook_widget().
 */
function canvas_field_widget(&$form, &$form_state, $field, $items, $delta = 0) {
  $element = array(
    '#type' => 'canvas_field',
    '#default_value' => isset($items[$delta]) ? $items[$delta] : NULL,
  );
  $settings['canvas_field'][$field['field_name'] .'['. $delta .'][value]'] = array(
    'width' => $field['widget']['canvas_width'],
    'height' => $field['widget']['canvas_height'],
  );
  drupal_add_js($settings, 'setting');
  return $element;
}



