
Drupal.behaviors.canvas_field = function(context) {
    $('input.canvas_field:not(.canvas_field-processed)').each(function() {
        	width = Drupal.settings.canvas_field[this.name].width;
        	height = Drupal.settings.canvas_field[this.name].height;
        	var canvas = new Canvas(height, width, this);
        	var wrapper = $('<div></div>');
        	wrapper.append(canvas);
        	var field = this;
        	
        	$(this).parents("form").submit(function() {
        	   field.value = canvas[0].toDataURL();
        	});
        	if(this.value) {
        	  deserialize(this.value, canvas[0]);
        	}

        	wrapper.insertAfter(this);
        	$(this).addClass('canvas_field-processed');
        
    });
    $('input.captured-canvas_field:not(.canvas_field-processed)').each(function() {
    	var i = $('<img>');
    	i.attr('src', this.value);
    	i.insertAfter(this);
    });
    
    document.addEventListener('touchstart', touchHandler, true);
    document.addEventListener('touchmove', touchHandler, true);
    document.addEventListener('touchend', touchHandler, true);
    document.addEventListener('touchcancel', touchHandler, true);    

}

function deserialize(data, canvas) {
  var img = new Image();
  img.onload = function() {
      canvas.width = img.width;
      canvas.height = img.height;
      canvas.getContext("2d").drawImage(img, 0, 0);
  };
  if(data.substring(0,5) == 'data:') {
    //Handle a dataurl
    img.src = data;
  } else {
    //Handle a filename
    img.src = Drupal.settings.basePath + data;
  }
}


Canvas = function(height, width, saveTo) {
	this.destination = saveTo;
	if(!width) { width = 600;}
	if(!height) {height = 150; }
    this.canvas = $('<canvas height="'+height+'" width="'+width+'"></canvas>');
    this.canvas.css("background-color", "gray");
    this.draw = false;
    this.origin = {'top':0, 'left':0};
    this.last = {'x':0, 'y':0};
    this.ctx=this.canvas[0].getContext("2d");
    var c = this;

    $(this.canvas)
    .mousedown(function (event) {c.onmousedown(this, event);})
    .mouseout(function(event) {return c.onmouseout(this, event);})
    .mousemove(function(event) {return c.onmousemove(this, event);});

    $(document).mouseup(function(event, saveTo) {return c.onmouseup(this, event);})
    return this.canvas;
}

Canvas.prototype.onmousedown = function(x, e) {
    this.draw = true;
}

Canvas.prototype.onmouseout = function(x, e) {
    this.last['x'] = 0;
    this.last['y'] = 0;
}
Canvas.prototype.onmouseup = function(x, e) {
    this.draw=false;
    this.last['x']=0;
    this.last['y']=0;
    //Note: Because of Drupal's crazy Ahah handling, we 
    //aren't able to attach this save anywhere else.
    this.destination.value = this.canvas[0].toDataURL();
}
Canvas.prototype.onmousemove = function(x, e) {
    if(this.draw==true){
        this.origin = this.canvas.offset();
        y = e.pageY-this.origin['top'];
        x = e.pageX-this.origin['left'];
        if(this.last['x']==0) {
            this.last['x'] = x+1;
            this.last['y'] = y+1;
        }
        this.ctx.beginPath();
        this.ctx.moveTo(x,y);
        this.ctx.lineTo(this.last['x'],this.last['y']);
        this.ctx.stroke();
        this.last['x'] = x;
        this.last['y'] = y;
    }
}

function touchHandler(event) {
    var touches = event.changedTouches,
        first = touches[0],
        type = '';
    
    switch(event.type)
    {
        case 'touchstart': type = 'mousedown'; break;
        case 'touchmove':  type='mousemove'; break;        
        case 'touchend':   type='mouseup'; break;
        default: return;
    }
    
    var simulatedEvent = document.createEvent('MouseEvent');
    simulatedEvent.initMouseEvent(type, true, true, window, 1, 
                              first.screenX, first.screenY, 
                              first.clientX, first.clientY, false, 
                              false, false, false, 0/*left*/, null);
                                                                            
    first.target.dispatchEvent(simulatedEvent);
    event.preventDefault();
}
