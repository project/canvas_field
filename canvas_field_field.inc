<?php
function canvas_field_field_validate($node, $field, $items, $teaser, $page) {
  if (is_array($items)) {
    foreach ($items as $delta => $item) {
      $error_element = isset($item['_error_element']) ? $item['_error_element'] : '';
      if (is_array($item) && isset($item['_error_element'])) unset($item['_error_element']);
    }
  }
  return $items;
}

function canvas_field_field_update(&$node, $field, &$items, $teaser, $page) {
  return canvas_field_field_insert($node, $field, $items, $teaser, $page);
}

function canvas_field_field_insert(&$node, $field, &$items, $teaser, $page) {
  foreach ($items as $delta => &$item) {
    if($data = canvas_field_parseDataURL($item['value'])) {
      //Get the destination filename.
      $dest_path = variable_get('canvas_field_directory', file_directory_path());
      $filename = $dest_path . '/canvas_field-'. $node->nid . $delta . '.' . $data->ext;
      if ($file = file_save_data(base64_decode($data->data), $filename)) {
        $item['value'] = $file;
      }
      else {
        unset($item['value']);
      }
    }
  }
}

/*
 * Helper function to parse data url and
 * validate that the string is well formed.
 *
 */
function canvas_field_parseDataURL($string) {
  $data = preg_split("/[,;:]+/", $string);
  switch ($data[1]) {
    case 'image/png':
      $ext = 'png';
      break;
    case 'image/jpg':
    case 'image/jpeg':
      $ext = 'jpg';
      break;
  }
  if (!$ext || $data[0] != 'data' || $data[2] != 'base64') {
    print_r($data);
    return FALSE;
  }
  $parsed = new stdClass();
  $parsed->data = $data[3];
  $parsed->ext = $ext;
  return $parsed;
}

function canvas_field_validDataURL($string) {
  $data = preg_split("/[,;:]+/", $string);
  switch ($data[1]) {
    case 'image/png':
      $ext = 'png';
      break;
    case 'image/jpg':
    case 'image/jpeg':
      $ext = 'jpg';
      break;
  }
  if (!$ext || $data[0] != 'data' || $data[2] != 'base64') {
    return TRUE;
  }
  return FALSE;
}